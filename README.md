#The No Agenda Art Generator

The No Agenda Art Generator has provided a simple way for producers of the No Agenda Show to upload high quality album
artwork for the hosts since mid-2010.  Originally built in Drupal 6, the Generator has served as a template for advanced
use of tools like the ImageCache and ImageCache Actions modules for that community.

However, it has gotten long in the tooth. This project moves the functionality of the old codebase to a more powerful and
simplified Laravel 4 project (this project's road map will upgrade to Laravel 5 once it reaches RC stage.)

You'll need Bower and Gulp for this to work correctly - [read more here](http://ryankent.ca/starting-a-new-project-with-laravel-5-gulp-bower-and-bootstrap/).