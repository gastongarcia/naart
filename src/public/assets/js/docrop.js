$(function() {
    function overlayswap() {
        var overlaychosen = $('#whichoverlay').find(':selected').data('overlay');
        //console.log('overlaychosen: ' + overlaychosen );
        if (overlaychosen === 'none') {
            $('#overlaygraphic').css('background-image', 'none');
            //console.log('removed overlaygraphic.');
        } else {
            $('#overlaygraphic').css('background-image', 'url("/assets/artwork/' + overlaychosen + '")');
            //console.log('added overlaygraphic: /assets/artwork/' + overlaychosen);
        }
    }
    overlayswap();
    $(document).on('change', '#whichoverlay', function(){
        overlayswap();
    });
    var wrapW = $('#preview-wrapper').width();
    var wrapH = wrapW;
    $('#preview-wrapper').width(wrapW);
    $('#preview-wrapper').height(wrapH);
    $('.preview-lg').width(wrapW);
    $('.preview-lg').height(wrapW);
    var $cropper = $(".cropper"),
        $dataX = $("#dataX"),
        $dataY = $("#dataY"),
        $dataH = $("#dataH"),
        $dataW = $("#dataW"),
        cropper;

    $cropper.cropper({
        aspectRatio: 1,
        data: {
            x: 50,
            y: 50,
            width: 256,
            height: 256
        },
        preview: ".preview",

        done: function(data) {
            var json = [
                '{"x":' + data.x,
                '"y":' + data.y,
                '"height":' + data.height,
                '"width":' + data.width + "}"
            ].join();
            //console.log(json);
            $('#img-data').val(json);
            $dataX.val(data.x);
            //console.log('dataX: ' + data.x);
            $dataY.val(data.y);
            //console.log('datay: ' + data.y);
            $dataH.val(data.height);
            //console.log('dataH: ' + data.height);
            $dataW.val(data.width);
            //console.log('dataW: ' + data.width);
        },
        build: function(e) {
            //console.log(e.type);
        },
        built: function(e) {
            //console.log(e.type);
        },
        dragstart: function(e) {
            //console.log(e.type);
        },
        dragmove: function(e) {
            //console.log(e.type);
        },
        dragend: function(e) {
            //console.log(e.type);
        }
    });

    cropper = $cropper.data("cropper");

    $cropper.on({
        "build.cropper": function(e) {
            //console.log(e.type);
            // e.preventDefault();
        },
        "built.cropper": function(e) {
            //console.log(e.type);
            // e.preventDefault();
        },
        "dragstart.cropper": function(e) {
            //console.log(e.type);
            // e.preventDefault();
        },
        "dragmove.cropper": function(e) {
            //console.log(e.type);
            // e.preventDefault();
        },
        "dragend.cropper": function(e) {
            //console.log(e.type);
            // e.preventDefault();
        }
    });

    $("#setData").click(function() {
        $cropper.cropper("setData", {
            x: $dataX.val(),
            y: $dataY.val(),
            width: $dataW.val(),
            height:$dataH.val()
        });
    });
});
