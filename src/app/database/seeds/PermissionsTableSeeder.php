<?php

class PermissionsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('permissions')->delete();

        $permissions = array(
            array( // 1
                'name'         => 'manage_artwork',
                'display_name' => 'manage artwork'
            ),
            array( // 2
                'name'         => 'manage_users',
                'display_name' => 'manage users'
            ),
            array( // 3
                'name'         => 'manage_roles',
                'display_name' => 'manage roles'
            ),
            array( // 4
                'name'         => 'post_artwork',
                'display_name' => 'post artwork'
            ),
            array( // 5
                'name'         => 'edit_own_artwork',
                'display_name' => 'edit own artwork'
            ),
        );

        DB::table('permissions')->insert( $permissions );

        DB::table('permission_role')->delete();

        $role_id_admin = Role::where('name', '=', 'admin')->first()->id;
        $role_id_user  = Role::where('name', '=', 'user')->first()->id;
        $permission_base = (int)DB::table('permissions')->first()->id - 1;

        $permissions = array(
            array(
                'role_id'       => $role_id_admin,
                'permission_id' => $permission_base + 1
            ),
            array(
                'role_id'       => $role_id_admin,
                'permission_id' => $permission_base + 2
            ),
            array(
                'role_id'       => $role_id_admin,
                'permission_id' => $permission_base + 3
            ),
            array(
                'role_id'       => $role_id_admin,
                'permission_id' => $permission_base + 4
            ),
            array(
                'role_id'       => $role_id_admin,
                'permission_id' => $permission_base + 5
            ),
            array(
                'role_id'       => $role_id_user,
                'permission_id' => $permission_base + 5
            ),
        );

        DB::table('permission_role')->insert( $permissions );
    }

}
