<?php

class Artwork extends Eloquent
{

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function overlay()
    {
        return $this->hasOne('Overlay');
    }

    public function episode()
    {
        return $this->belongsTo('Episode');
    }

}
