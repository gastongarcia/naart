@extends('layouts.master')

@section('title')@parent:: Signup or Login @stop

@section('content')
    <div class="container">
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif

        @if (Session::get('notice'))
            <div class="alert">{{ Session::get('notice') }}</div>
        @endif
        <div class="row" style="margin-top:1em;">
            <div class="well bg-info text-info">
                <h3 style="margin-top:0;">Welcome!</h3>
                <p>If you already had a No Agenda Art Generator Username and Password and had submitted any artwork, you can retrieve your account using the forgot password link below.  The old site and this one both use a salted and hashed password protection that prevents even the administrators from retrieving your password.  Long randomized passwords were generated to protect your existing accounts.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h2>Login to Your Account</h2>
                <form role="form" method="POST" class="form-horizontal form-auth" action="{{{ URL::to('/users/login') }}}" accept-charset="UTF-8">
                    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">{{{ Lang::get('confide::confide.username_e_mail') }}}</label>
                        <div class="col-sm-9">
                            <input class="form-control" tabindex="1" placeholder="{{{ Lang::get('confide::confide.username_e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="password">
                            {{{ Lang::get('confide::confide.password') }}}
                        </label>
                        <div class="col-sm-9">
                            <input class="form-control compressed" tabindex="2" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password">
                            <p class="help-block">
                                <a href="{{{ URL::to('/users/forgot_password') }}}">{{{ Lang::get('confide::confide.login.forgot_password') }}}</a>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox col-sm-3 col-sm-offset-3">
                            <label class="control-label" for="remember">
                                <input type="hidden" name="remember" value="0">
                                <input tabindex="4" type="checkbox" name="remember" id="remember" value="1"> {{{ Lang::get('confide::confide.login.remember') }}}
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <button tabindex="3" type="submit" class="btn btn-primary btn-default">{{{ Lang::get('confide::confide.login.submit') }}}</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <h2>Create a New Account</h2>
                <form method="POST" class="form-horizontal form-auth" action="{{{ URL::to('users') }}}" accept-charset="UTF-8" role="form">
                    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="username">{{{ Lang::get('confide::confide.username') }}}</label>
                        <div class="col-sm-9">
                            <input class="form-control" placeholder="{{{ Lang::get('confide::confide.username') }}}" type="text" name="username" id="su_username" value="{{{ Input::old('username') }}}">
                        </div>
                    </div>
                    <div class="form-group compressed">
                        <label class="col-sm-3 control-label" for="email">{{{ Lang::get('confide::confide.e_mail') }}}</label>
                        <div class="col-sm-9">
                            <input class="form-control" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text" name="email" id="su_email" value="{{{ Input::old('email') }}}">
                            <p class="help-block"><span class="text-danger">{{ Lang::get('confide::confide.signup.confirmation_required') }}</span></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="password">{{{ Lang::get('confide::confide.password') }}}</label>
                        <div class="col-sm-9">
                            <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="su_password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="password_confirmation">{{{ Lang::get('confide::confide.password_confirmation') }}}</label>
                        <div class="col-sm-9">
                            <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" type="password" name="password_confirmation" id="su_password_confirmation">
                        </div>
                    </div>

                    <div class="form-actions form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary">{{{ Lang::get('confide::confide.signup.submit') }}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
