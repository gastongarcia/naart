<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'sitename' => 'No Agenda Art Generator',
    'artwork'  => array(
        'create_title' => 'Create Your New Artwork Submission',
        'success'      => 'Your artwork has been submitted - good luck in the selection process!',
        'updated'      => 'The overlay has been successfully updated.',
        'cannotupdate' => 'Cannot update overlay, either you do not have permission, or the artwork has been archived.',
        'artdeleted'   => 'We deleted the artwork, it will be sorely missed.',
    ),
    'artist'  => array(
        'doesntexist'  => 'The artist you provided is not a valid user.',
    ),

);
